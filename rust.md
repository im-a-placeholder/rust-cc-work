# Rust

My cheat sheet from reading the [rust book](https://doc.rust-lang.org/book/).



[TOC]





## 1. Getting started



### 1.1. Installation

Linux & macOS

```bash
$ curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh
```



[Windows](https://www.rust-lang.org/tools/install)



Updating and uninstalling

```bash
rustup update
```

```bash
rustup self uninstall
```



Troubleshooting

```bash
rustc --version
```



Local doc

```bash
rustup doc
```



### 1.2. Hello, World!

Creating a project directory:

- Linux

```bash
$ mkdir ~/projects
$ cd ~/projects
$ mkdir hello_world
$ cd hello_world
```

- Windows

```bash
> mkdir "%USERPROFILE%\projects"
> cd /d "%USERPROFILE%\projects"
> mkdir hello_world
> cd hello_world
```



Writing and running a Rust program:

```rust
// main.rs
fn main() {
    println!("Hello, world!");
}
```

```bash
# on Linux or macOS
$ rustc main.rs
$ ./main
Hello, world!
```

```bash
# on Windows
> rustc main.rs
> .\main.exe
Hello, world!
```



Anatomy of a Rust program:

```rust
// Always the first function to run in Rust.
fn main() {
    // Print the given string, the ending '!' means that it calls a macro not a function.
    // indents with 4 spaces.
    println!("Hello, world!");
}
```



### 1.3. Hello, Cargo!

Check cargo presence:

```bash
cargo --version
```



Creating a Project with Cargo:

```bash
$ cargo new hello_cargo
$ cd hello_cargo
```

Creates:

```
hello_cargo
│   .gitignore
│   Cargo.toml   
│
└───.git
│   │   ...
│   
└───src
    │   main.rs
```

```toml
# Cargo.toml
[package]
name = "hello_cargo"
version = "0.1.0"
authors = ["Your Name <you@example.com>"]
edition = "2018"

[dependencies]
```



Building and running a Cargo project:

```bash
$ cargo build
   Compiling hello_cargo v0.1.0 (file:///projects/hello_cargo)
    Finished dev [unoptimized + debuginfo] target(s) in 2.85 secs
```

```bash
$ ./target/debug/hello_cargo # or .\target\debug\hello_cargo.exe on Windows
Hello, world!
```

```bash
$ cargo run # build and run
    Finished dev [unoptimized + debuginfo] target(s) in 0.0 secs
     Running `target/debug/hello_cargo`
Hello, world!
```

```bash
$ cargo check # just check if it can compile
   Checking hello_cargo v0.1.0 (file:///projects/hello_cargo)
    Finished dev [unoptimized + debuginfo] target(s) in 0.32 secs
```



Building for release:

```bash
cargo build --release
```



Cloning a project:

```bash
$ git clone someurl.com/someproject
$ cd someproject
$ cargo build
```





## 2. Programming a guessing game



Setting up a new project:

```bash
$ cargo new guessing_game
$ cd guessing_game
```



```rust
// main.rs

use rand::Rng; // Add the Rng trait.
use std::cmp::Ordering; // enum with these variants: Less, Greater and Equal.
use std::io; // Input/output library (io) from the standard library (std).

// Program entry point.
fn main() {
    
    // Prints a string, variables can be placed with curly braces: println!("Some text, var: {}", my_var); .
    println!("Guess the number!");

    // rand::thread_rng is the random number generator that we're going to use, we use gen_range on it (inclusive on the lower bound, exclusive on the upper).
    let secret_number = rand::thread_rng().gen_range(1, 101);

    loop {
        println!("Please input your guess.");

        // Instantiate an empty mutable variable with an empty string.
        let mut guess = String::new();

        // "a handle to the standard input for your terminal"
        io::stdin()
            // Copy user input to the mutable reference (&) of the guess variable, then return an io::Result enum (in this case: "Ok" or "Err").
            .read_line(&mut guess)
            // If read_line() returns an "Err", the program will crash and display the provided message.
            .expect("Failed to read line");

        // Converts (by shadowing) the guess value in an unsigned 32 bit integer.
        // A match expression is made up of arms: the result of guess.trim().parse() will be matched with the corresponding arm.
        // trim(): trims the white space and newline (\n) caused by pressing Enter.
        // When parsing strings to numbers the type must be explicitly set ex: u32.
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            // "continue" will send the program to the next loop iteration.
            Err(_) => continue,
        };

        println!("You guessed: {}", guess);
        
        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small"),
            Ordering::Greater => println!("Too big"),
            Ordering::Equal => {
                println!("You win!");
                // Break the loop.
                break;
            },
        }
    }
}
```





## 3. Common programming concepts



### 3.1. Variables and mutability

```rust
fn main() {
    let spaces = "   "; // Immutable.
    let spaces = spaces.len(); // Here spaces is shadowed because we used 'let'.

	let mut y = 7; // Use mut when necessary.
	y = 10; // We can reassign y with other numbers.
    
    // Constants names are uppercase and spaces are underscores, underscores can be used in numerical values for readability.
    const BIG_NUMERAL: u32 = 1_000;
}
```



### 3.2. Data types

#### Scalar types

A scalar type represents a single value.



##### Integers

| Length  | Signed  | Unsigned |
| ------- | ------- | -------- |
| 8-bit   | `i8`    | `u8`     |
| 16-bit  | `i16`   | `u16`    |
| 32-bit  | `i32`   | `u32`    |
| 64-bit  | `i64`   | `u64`    |
| 128-bit | `i128`  | `u128`   |
| arch    | `isize` | `usize`  |

Signed range: [ -(2<sup>n-1</sup>) .. 2<sup>n-1</sup> -1 ]

Unsigned range: [ 0 .. 2<sup>n</sup> -1 ]

isize depends on the computer architecture, 64-bit or 32-bit.

The doc states that for types: "Rust’s defaults are generally good choices".



##### Floating-Point types

`f32` & `f64`



##### Booleans

`true` or `false`



##### Characters

```rust
fn main() {
    let c = 'z';
    let z = 'ℤ';
    let heart_eyed_cat = '😻';
}
```



#### Compound type

*Compound types* can group multiple values into one type. 



##### Tuples

A tuple is a general way of grouping together a number of values with a variety of types into one compound type. Tuples have a fixed length: once declared, they cannot grow or shrink in size.

```rust
fn main() {
    let tup: (i32, f64, u8) = (500, 6.4, 1);
    let five_hundred = tup.0;
    // Destructuring.
    let (x, y, z) = tup;
}
```



##### Arrays

Like tuples, arrays have a fixed number of elements, but must all be of the same type.

```rust
let months = ["January", "February", "March", "April", "May", "June", "July",
              "August", "September", "October", "November", "December"];
let february = months[1];
let a: [i32; 5] = [1, 2, 3, 4, 5];
let a = [3; 5]; // Same as: let a = [3, 3, 3, 3, 3];
```



### 3.3. Functions

#### Parameters, statements & expressions

*Statements* are instructions that perform some action and do not return a value. *Expressions* evaluate to a resulting value.

Expressions do not include ending semicolons.

```rust
fn main() {

    let x = {
        // The content of this block is an expression that evaluates to 4.
        let x = 3;
        x + 1
    };

    another_function(x, 5);
}

fn another_function(x: i32, y: i32) {
    println!("The value of x is: {}", x);
    println!("The value of y is: {}", y);
}
```

```rust
fn main() {
    // The following line is a statement, but 6 is an expression that evaluates to 6.
    let y = 6;
}
```

```rust
fn main() {
    // This won't compile because statements don't return any values.
    let x = (let y = 6);
}
```



#### Return values

Functions that return values have this syntax: `fn function_name() -> type {}`. The return value is most often the value of the final expression, but values can sometime be returned early by using the `return` keyword.

```rust
fn main() {
    let x = plus_one(5);

    println!("The value of x is: {}", x);
}

fn plus_one(x: i32) -> i32 {
    x + 1
}
```

Statements don’t evaluate to a value, which is expressed by `()`, an empty tuple.



### 3.5. Control flow

#### If expressions

```rust
fn main() {
    let number = 3;

    if number < 5 {
        println!("condition was true");
    } else {
        println!("condition was false");
    }
}
```

Blocks of code associated with the conditions in `if` expressions are sometimes called *arms*, just like the arms in `match` expressions.



#### Handling multiple conditions with else if

```rust
fn main() {
    let number = 6;

    if number % 4 == 0 {
        println!("number is divisible by 4");
    } else if number % 3 == 0 {
        println!("number is divisible by 3");
    } else if number % 2 == 0 {
        println!("number is divisible by 2");
    } else {
        println!("number is not divisible by 4, 3, or 2");
    }
}
```

`number is divisible by 3`

```rust
fn main() {
    let condition = true;
    // number can only have one type, so here the condition arms must all be of same type.
    let number = if condition { 5 } else { 6 };

    println!("The value of number is: {}", number);
}
```



#### Repetition with loops

##### Repeating code with loop

```rust
fn main() {
    // Will print "again!" continuously, 'break' can be used to exit the loop.
    loop {
        println!("again!");
    }
}
```



##### Returning Values from Loops

```rust
fn main() {
    let mut counter = 0;

    let result = loop {
        counter += 1;

        if counter == 10 {
            break counter * 2;
        }
    }; // Ending semicolon, a variable declaration is a statement.

    // result value is 20.
    println!("The result is {}", result);
}
```



##### Conditional Loops with while

```rust
fn main() {
    let mut number = 3;

    while number != 0 {
        println!("{}!", number);

        number -= 1;
    }

    println!("LIFTOFF!!!");
}
```

```bash
3!
2!
1!
LIFTOFF!!!
```



##### Looping through a collection with for

```rust
fn main() {
    let a = [10, 20, 30, 40, 50];
    let mut index = 0;

    while index < 5 {
        println!("the value is: {}", a[index]);

        index += 1;
    }
}
```

```bash
the value is: 10
...
the value is: 50
```

The above code is error prone and slow, use 'for in' for the same output:

```rust
fn main() {
    let a = [10, 20, 30, 40, 50];

    for element in a.iter() {
        println!("the value is: {}", element);
    }
}
```

Or even:

```rust
fn main() {
    for number in (1..4).rev() {
        println!("{}!", number);
    }
    println!("LIFTOFF!!!");
}
```

```bash
3!
2!
1!
LIFTOFF!!!
```





## 4. Understanding Ownership

**Most examples won't include the main function any more.**



### 4.1. What Is Ownership?

Memory management:

- The stack: data has a known, fixed size, last in, first out. Adding data: pushing onto the stack. Removing data: popping off the stack.
- The heap: data with an unknown size at compile time or a size that might change must be stored on the heap. The memory allocator finds an empty spot in the heap that is big enough, marks it as being in use, and returns a pointer (address of that location). This is called allocating on the heap. The pointer can be stored on the stack. Allocating data or retrieving it is slower than pushing onto the stack.



#### Ownership rules

- Each value in Rust has a variable that’s called its *owner*.
- There can only be one owner at a time.
- When the owner goes out of scope, the value will be dropped.



#### Variable scope

Variable scope work the same way for values stored in the stack or the heap:

```rust
{                      // s is not valid here, it’s not yet declared.
    let s = "hello";   // s is valid from this point forward.

    // Do stuff with s.
}                      // This scope is now over, and s is no longer valid.
```

When a variable goes out of the scope, Rust automatically calls the drop function to drop the associated value from memory.



#### The String type

```rust
let mut s = String::from("hello"); // Create a String with the string litteral "hello". s's value is allocated on the heap.

s.push_str(", world!"); // push_str() appends a literal to a String.

println!("{}", s); // This will print.
```

`hello, world!`



#### Ways variables and data interact: move

```rust
let s1 = String::from("hello");
let s2 = s1; // s1 is moved to s2, not shadowed.

println!("{}, world!", s1); // This will throw an error, s1 is invalidated.
```
<img src="img/trpl04-04.svg" style="width: 500px">



#### Ways variables and data interact: clone

```rust
let s1 = String::from("hello");
let s2 = s1.clone();

println!("s1 = {}, s2 = {}", s1, s2);
```

`s1 = hello, s2 = hello`

<img src="img/trpl04-03.svg" style="width: 500px">



#### Stack-only data: copy

```rust
let x = 5;
let y = x;

println!("x = {}, y = {}", x, y); // This works without clonning because integers are stored on the stack and implement the Copy trait.
```

Common types that are `Copy`:

- All the integer types, such as `u32`.
- The Boolean type, `bool`, with values `true` and `false`.
- All the floating point types, such as `f64`.
- The character type, `char`.
- Tuples, if they only contain types that are also `Copy`. For example, `(i32, i32)` is `Copy`, but `(i32, String)` is not.



#### Ownership and functions

```rust
fn main() {
    let s = String::from("hello");  // s comes into scope

    takes_ownership(s);             // s's value moves into the function...
                                    // ... and so is no longer valid here

    let x = 5;                      // x comes into scope

    makes_copy(x);                  // x would move into the function,
                                    // but i32 is Copy, so it’s okay to still
                                    // use x afterward

} // Here, x goes out of scope, then s. But because s's value was moved, nothing
  // special happens.

fn takes_ownership(some_string: String) { // some_string comes into scope
    println!("{}", some_string);
} // Here, some_string goes out of scope and `drop` is called. The backing
  // memory is freed.

fn makes_copy(some_integer: i32) { // some_integer comes into scope
    println!("{}", some_integer);
} // Here, some_integer goes out of scope. Nothing special happens.
```



#### Return values and scope

```rust
fn main() {
    let s1 = gives_ownership();         // gives_ownership moves its return
                                        // value into s1

    let s2 = String::from("hello");     // s2 comes into scope

    let s3 = takes_and_gives_back(s2);  // s2 is moved into
                                        // takes_and_gives_back, which also
                                        // moves its return value into s3
} // Here, s3 goes out of scope and is dropped. s2 goes out of scope but was
  // moved, so nothing happens. s1 goes out of scope and is dropped.

fn gives_ownership() -> String {             // gives_ownership will move its
                                             // return value into the function
                                             // that calls it

    let some_string = String::from("hello"); // some_string comes into scope

    some_string                              // some_string is returned and
                                             // moves out to the calling
                                             // function
}

// takes_and_gives_back will take a String and return one
fn takes_and_gives_back(a_string: String) -> String { // a_string comes into
                                                      // scope

    a_string  // a_string is returned and moves out to the calling function
}
```



```rust
fn main() {
    let s1 = String::from("hello");

    let (s2, len) = calculate_length(s1);

    println!("The length of '{}' is {}.", s2, len);
}

fn calculate_length(s: String) -> (String, usize) {
    let length = s.len(); // len() returns the length of a String

    (s, length)
}
```



### 4.2. References and Borrowing

```rust
fn main() {
    let s1 = String::from("hello");

    let len = calculate_length(&s1);

    println!("The length of '{}' is {}.", s1, len);
}

fn calculate_length(s: &String) -> usize { // s is a reference to a String   
    s.len()
} // Here, s goes out of scope. But because it does not have ownership of what it refers to, nothing happens.
```

We call having references as function parameters *borrowing*.



#### Mutable References

```rust
let mut s = String::from("hello");

let r1 = &s; // No problem.
let r2 = &s; // No problem.
println!("{} and {}", r1, r2);
// r1 and r2 are no longer used after this point

{
    let r4 = &mut s;
} // r4 goes out of scope here, so we can make a new reference with no problems.

let r3 = &mut s; // No problem, there is no simultaneous mutable references.
println!("{}", r3);
```



#### Dangling References

A dangling pointer is a pointer that references a location in memory that may have been given to someone else, by freeing some memory while preserving a pointer to that memory.

```rust
// An (nonworking) attempt of a dangling reference in Rust.
fn dangle() -> &String { // dangle returns a reference to a String

    let s = String::from("hello"); // s is a new String

    &s // we return a reference to the String, s
} // Here, s goes out of scope, and is dropped. Its memory goes away.
  // Danger!
```



#### The Rules of References

- At any given time, you can have *either* one mutable reference *or* any number of immutable references.
- References must always be valid.



### 4.3. The Slice Type

A program that takes a String and returns the index of the end of the first word:

```rust
fn main() {
    let mut s = String::from("hello world");

    let word = first_word(&s); // word will get the value 5

    s.clear(); // this empties the String, making it equal to ""

    // word still has the value 5 here, but there's no more string that
    // we could meaningfully use the value 5 with. word is now totally invalid!
}

fn first_word(s: &String) -> usize {
    let bytes = s.as_bytes(); // Requiered to iterate over the String element by element.

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        }
    }

    // If no space is found, return the String length.
    s.len()
}
```

word is not linked to s, but Rust has a solution to this problem: string slices.



#### String Slices

```rust
let s = String::from("hello world");

// The starting index is the first position in the slice and the ending index is one more than the last position in the slice.
let hello = &s[0..5];
let hello = &s[..5]; // Same as above.
let world = &s[6..11];
let world = &s[6..];
```

<img src="img/trpl04-06.svg" style="width: 500px">

```rust
fn first_word(s: &String) -> &str { // &str is the string slice type.
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }
    
	// If no space is found, return the String length.
    &s[..]
}
```



#### String Literals Are Slices

```rust
// The type of s is &str: it’s a slice pointing to that specific point of the binary.
let s = "Hello, world!";
```



#### String Slices as Parameters

```rust
fn first_word(s: &String) -> &str {
```

Can be rewritten to:

```rust
fn first_word(s: &str) -> &str {
```

It now can be used on both `&String` values and `&str` values.

```rust
fn main() {
    let my_string = String::from("hello world");

    // first_word works on slices of `String`s
    let word = first_word(&my_string[..]);

    let my_string_literal = "hello world";

    // first_word works on slices of string literals
    let word = first_word(&my_string_literal[..]);

    // Because string literals *are* string slices already,
    // this works too, without the slice syntax!
    let word = first_word(my_string_literal);
}
```



#### Other slices

```rust
let a = [1, 2, 3, 4, 5];

let slice = &a[1..3];
```





## 5. Using structs to structure related data



### 5.1. Defining and instantiating structs

```rust
// Define the User struct.
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

// Instantiate the User struct.
let mut user1 = User { // The instance is either mutable or not, fields mutability can't be set individually.
    email: String::from("someone@example.com"),
    username: String::from("someusername123"),
    active: true,
    sign_in_count: 1,
};

// Change the email field value.
user1.email = String::from("anotheremail@example.com");
```

The whole instance is either mutable or not.

```rust
// This function returns a User struct with the given email and username fields.
fn build_user(email: String, username: String) -> User {
    User {
        email, // Shorthand for email: email.
        username,
        active: true,
        sign_in_count: 1,
    }
}
```



#### Creating instances from other instances with struct update syntax

```rust
let user2 = User {
    email: String::from("another@example.com"),
    username: String::from("anotherusername567"),
    ..user1 // Shorthand for => active: user1.active, sign_in_count: user1.sign_in_count,
};
```



#### Using tuple structs without named fields to create different types

```rust
struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

let black = Color(0, 0, 0);
let origin = Point(0, 0, 0);
```

Tuple structs are similar to ordinary tuples but they have their own type, black and origin have a different type.



#### Unit-Like structs without any fields

Struct can also be defined without any fields, they will then behave similarly to `()`, the unit type. Unit-like structs can be useful in situations in which you need to implement a trait on some type but don’t have any data that you want to store in the type itself. 



#### Ownership of struct data

For the User struct we used a String type rather than &str, this is deliberate in order to insure that all instances will own their data and the data will be valid for the instance lifetime.

Structs can store references but then need to use lifetimes which will be discussed in chapter 10.